// function sum2(a, b) {
//   return a + b;
// }

function sum(a, b) {
  try {
    if (typeof a !== 'number' || typeof b !== 'number') {
      console.log('not number');
      throw new Error('Not a number');
    }
    if (b < a) {
      const temp = b;
      b = a;
      a = temp;
    }
    if (b > 0) {
      for (let i = 0; i < b; ++i) {
        a++;
      }
      return a;
    } else {
      for (let i = 0; i > b; --i) {
        a--;
      }
      return a;
    }
  } catch (err) {
    console.log(err);
    throw new Error('Some error la');
  }
}

module.exports = {
  sum,
};
